﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

public class ButtonUIScript : MonoBehaviour, IPointerClickHandler, IDragHandler, IPointerEnterHandler, IPointerExitHandler {

	public string SceneOnClick;
	public GameObject background;
	public float speedFade;

	private bool fadeIn, fadeOut;

	// Use this for initialization
	void Start () {
		if (speedFade == 0) {
			speedFade = 0.1f;
		}
		background.GetComponent<RawImage> ().color = new Color (background.GetComponent<RawImage> ().color.r, background.GetComponent<RawImage> ().color.g, background.GetComponent<RawImage> ().color.b, 0);
	}
	
	// Update is called once per frame
	void Update () {
		RawImage image = background.GetComponent<RawImage> ();
		if (fadeIn && image.color.a != 1) {
			image.color = new Color (image.color.r, image.color.g, image.color.b, image.color.a + speedFade);
		} else {
			fadeIn = false;
		}

		if (fadeOut && image.color.a != 0) {
			image.color = new Color (image.color.r, image.color.g, image.color.b, image.color.a - speedFade);
		} else {
			fadeOut = false;
		}


	
	}

	public void OnPointerEnter(PointerEventData eventData){
		background.GetComponent<RawImage> ().CrossFadeAlpha (1, 1, false);
		fadeIn = true;
	}

	public void OnPointerExit(PointerEventData eventData){
		background.GetComponent<RawImage> ().CrossFadeAlpha (0, 1, false);
		fadeOut = true;
	}


	public void OnPointerClick(PointerEventData eventData){
		Application.LoadLevel (SceneOnClick);
	}

	public void OnDrag(PointerEventData eventData){
		;
	}
}
