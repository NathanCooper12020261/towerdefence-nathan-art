﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FlyingEnemy : Enemy {

	public Rigidbody body;
	public ParticleSystem smoke;

	float originalHeight;
	// Use this for initialization
	public override void Start () {
		type = "Flying";
		fundsWorth = 100;
		currentHealth = maxHealth;
		originalHeight = this.transform.position.y;

		base.Start ();
	}
	

}
