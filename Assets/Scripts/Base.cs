﻿using UnityEngine;
using UnityEngine.Networking;

public class Base : BaseHealth {
	public GameObject explosion;

	void Awake () {
		currentHealth = maxHealth;
	}
    public override void OnStartClient() {
        base.OnStartClient();
        currentHealth = maxHealth;

    }

	void Update(){
		if (currentHealth < maxHealth / 3) {
			if (!gameObject.GetComponent<AudioSource> ().isPlaying) {
				gameObject.GetComponent<AudioSource> ().Play ();
			}
		}
	}

    [Server]
	protected override void Die(){
        //here goes explode stuff + stuff which should happen after explosion
       // CmdDebug("baseded");
		GameManager.singleton.state = GameManager.GameState.LOST;  

		GameObject exp = (GameObject)GameObject.Instantiate(explosion, transform.position, new Quaternion());
		NetworkServer.Spawn(exp);

		base.Die ();
	}


}
