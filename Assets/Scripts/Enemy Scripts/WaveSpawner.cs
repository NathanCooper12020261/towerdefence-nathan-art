﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

public class WaveSpawner : NetworkBehaviour {

	public class Wave {
        public List<Enemy> enemies;
		public Vector3 spawnPosition = Vector3.zero;
		public float spawnRadius = 5;
		public float rate = 1;

		public Wave(){

		}
		public Wave(List<Enemy> enemies, Vector3 spawnPosition, float spawnRadius, float rate){
			this.enemies = enemies;
			this.spawnPosition = spawnPosition;
			this.spawnRadius = spawnRadius;
			this.rate = rate;
		}
    }
    [SerializeField]
    private Transform waveSpawnPos;
    [SerializeField]
    private float waveSpawnRadius;
    [SerializeField]
    private float waveSpawnRate;

    public List<Enemy> enemies;
   
    public enum SpawnState { SPAWNING, WAITING };
	public SpawnState state;
	bool newWave = true;
	float readyTime = 5f;
	float spawnCountdown = 0;
	
	public List<Wave> waves;
	int nextWave = 0;
	public int tempSetupNoOfEnemies = 5;
    WaveSpawner singleton;
    [ClientRpc]
    void RpcSetup() {

    }
	// Use this for initialization
	void Start () {
        if (!isServer) {
            return;
        }
		waves = new List<Wave> ();

		//RIO - ADDING DUMMY STAGES
		//waves.Add(new Wave(enemies, ));
		//waves.Add (new Wave ());
		//waves.Add (new Wave ());
		//Waves set up

		TempSetup (tempSetupNoOfEnemies);
		state = SpawnState.WAITING;

	}
	void TempSetup(int number){
		List<Enemy> waveEnemies = new List<Enemy> ();
        waveEnemies.Add(enemies[0]);
        waveEnemies.Add(enemies[0]);
        waveEnemies.Add(enemies[0]);
        waveEnemies.Add(enemies[0]);
        waveEnemies.Add(enemies[0]);
        waveEnemies.Add(enemies[0]);
        waveEnemies.Add(enemies[0]);
        waveEnemies.Add(enemies[0]);
        waveEnemies.Add(enemies[0]);
        waveEnemies.Add(enemies[0]);
        waves.Add(new Wave(waveEnemies, waveSpawnPos.position, waveSpawnRadius, waveSpawnRate));
        waveEnemies.Add(enemies[0]);
        waveEnemies.Add(enemies[0]);
        waveEnemies.Add(enemies[0]);
        waveEnemies.Add(enemies[0]);
        waveEnemies.Add(enemies[0]);
        waves.Add(new Wave(waveEnemies, waveSpawnPos.position, waveSpawnRadius, waveSpawnRate));
        waveEnemies.Add(enemies[0]);
        waveEnemies.Add(enemies[0]);
        waveEnemies.Add(enemies[0]);
        waveEnemies.Add(enemies[0]);
        waveEnemies.Add(enemies[0]);
        waveEnemies.Add(enemies[0]);
        waveEnemies.Add(enemies[0]);
        waveEnemies.Add(enemies[0]);
        waveEnemies.Add(enemies[0]);
        waveEnemies.Add(enemies[0]);
        waves.Add(new Wave(waveEnemies, waveSpawnPos.position, waveSpawnRadius, waveSpawnRate));
    }
	// Update is called once per frame
    
	void Update () {
        if (!isServer) return;
		if (GameManager.singleton.state == GameManager.GameState.PHASE_FIGHTING) {
			if(state != SpawnState.SPAWNING && newWave){
                print("wave");
				newWave= false;
				StartCoroutine (SpawnEnemies(waves[nextWave]));
				nextWave++;
			} else if(state != SpawnState.SPAWNING && !IsEnemyAlive()){
				if (nextWave >= waves.Count) {
					GameManager.singleton.state = GameManager.GameState.WON;
				} else {
					newWave = true;
					GameManager.singleton.state = GameManager.GameState.PHASE_BUILDING;
					GameManager.singleton.nextPhaseCountdown = GameManager.buildTime;
				}
			}	
		}
	}

	IEnumerator SpawnEnemies(Wave wave) {
        print("wave1");
        state = SpawnState.SPAWNING;
		for (int j = 0; j < wave.enemies.Count; j++) {
            EnemyManager.singleton.SpawnEnemy(wave.enemies[j].type, wave.spawnPosition, wave.spawnRadius);
            print("wave2");
            yield return new WaitForSeconds(wave.rate);
            
        }
        state = SpawnState.WAITING;

        yield break;
	}
	bool IsEnemyAlive(){
		return GameObject.FindGameObjectWithTag ("Enemy") != null ? true : false;
	}

	#region tempGridGizmosLocation
	void OnDrawGizmos(){
		if (Grid.singleton == null)
			return;
		if (Grid.grid != null && GameObject.FindGameObjectsWithTag ("Player").Length > 0) {
			//there are more than one players - how will this work with the Grid?
			List<GameObject> players = new List<GameObject>(GameObject.FindGameObjectsWithTag ("Player"));

			GridNode playerNode = Grid.GetNode (players[0].transform.position);

			foreach(GridNode gn in Grid.grid){
				Gizmos.color = gn.walkable ? Color.white : Color.red;
				Gizmos.color = (playerNode == gn) ? Color.green : Gizmos.color;

				Gizmos.DrawWireCube(gn.worldPos, new Vector3(Grid.nodeDiameter, 1, Grid.nodeDiameter));

			}
		}
	}
	#endregion
}
