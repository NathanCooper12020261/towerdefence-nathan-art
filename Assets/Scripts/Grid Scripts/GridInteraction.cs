﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections.Generic;

public class GridInteraction : NetworkBehaviour {

    #region Fields

    public static Dictionary<string, Tower> spawnableTowers = new Dictionary<string, Tower>();
    public List<Tower> spawnableTowersList = new List<Tower>();
    GameObject[] waveSpawnPositions;

    string currentTowerType = Tower.TRIGUN;
    public bool gridInteractionEnabled;
    List<Vector3> currentNodes;
    Vector3 instanPos = Vector3.zero;

    private string[] currentTowerTypes = {
        Tower.TRIGUN,
        Tower.PLASMA,
        Tower.ELECTRIC
    };
	private int currentTowerTypeIndex = 0;

    Tower towerSlot;
    public Tower currentTower {
        //get { return spawnableTowers[currentTowerType]; }
		get { return spawnableTowers [currentTowerTypes [currentTowerTypeIndex]]; }
	}
    

    [SerializeField]
    Material wireBoxMat;
    GameObject wireBox; 
    Camera mainCamera;
    Player player;
    RaycastHit hit;
    Pathfinder pathfinder;
    Transform endZone;
    #endregion

    #region Main Functions
    public override void OnStartClient() {
        if (spawnableTowers.Count > 0) return;
        for (int i = 0; i < spawnableTowersList.Count; i++) {
            print(i);
			string type = spawnableTowersList [i].type;
			Tower tower = spawnableTowersList [i];
            spawnableTowers.Add(spawnableTowersList[i].type, spawnableTowersList[i]);
        }

		
		currentTowerTypeIndex = 0;
    }
    void Start() {
        pathfinder = new Pathfinder();
        currentNodes = new List<Vector3>();
        player = GetComponent<Player>();
        gridInteractionEnabled = false;
        hit = new RaycastHit();
        endZone = GameObject.FindGameObjectWithTag("Base").transform;
        if (isClient) {
            mainCamera = FindCamera();
            wireBox = GameObject.CreatePrimitive(PrimitiveType.Cube);
            wireBox.GetComponent<MeshRenderer>().material = wireBoxMat;
            wireBox.GetComponent<BoxCollider>().enabled = false;
            wireBox.SetActive(false);
            wireBox.transform.localScale = new Vector3(Grid.nodeRadius * 4, .3f, Grid.nodeRadius * 4);
        }
        waveSpawnPositions = GameObject.FindGameObjectsWithTag("SpawnPos");

    }
    void Update() {
        if (!isLocalPlayer)
            return;

        if (GameManager.singleton.state != GameManager.GameState.PHASE_BUILDING) {
            gridInteractionEnabled = false;
            wireBox.SetActive(false);
            return;
        }



        if (gridInteractionEnabled) {
            wireBox.SetActive(true);
            // We need to actually hit an object
            if (
                !Physics.Raycast(mainCamera.ScreenPointToRay(Input.mousePosition).origin,
                    mainCamera.ScreenPointToRay(Input.mousePosition).direction, out hit, 4,
                    Physics.DefaultRaycastLayers)) {
                wireBox.SetActive(false);
                return;
            }
            // Check that we are hitting a gameobject with tag Ground as thats where nodes are kinda...
            if (hit.collider.gameObject.tag != "Ground") {
                wireBox.SetActive(false);
                return;
            }
            // Set instanPos and currentNodes   TODO: if we experience lag problems this might not be very performance friendly
            if (!GetCurrentNodes()) {
                wireBox.SetActive(false);
                return;
            }

            // activate and move wirebox
            if (!wireBox.activeSelf)
                wireBox.SetActive(true);
            wireBox.transform.position = instanPos;
        } else {
            if (wireBox.activeSelf) {
                wireBox.SetActive(false);
            }
        }
    }
   

	public void EnableGridInteraction(){
		gridInteractionEnabled = !gridInteractionEnabled;
		if(wireBox != null) wireBox.SetActive(!wireBox.activeSelf);
	}

	public void EnableGridInteraction(bool value){
		gridInteractionEnabled = value;
		if(wireBox != null) wireBox.SetActive(value);
	}

	public bool BuildTower(){
        foreach(GameObject g in waveSpawnPositions) {
            if(pathfinder.IsBlockingPath(Grid.GetVector2(g.transform.position), Grid.GetVector2(endZone.position))) {
                Debug.Log("Cannot place tower as would block enemy pathfinding");
                return false;
            }
        }
		if (wireBox.activeSelf) {
			if (player.funds >= currentTower.price) {

				string playerID = player.transform.name;
				print(playerID);
				Vector3 towerSpawn = new Vector3 (instanPos.x, instanPos.y - 3, instanPos.z);

				CmdSpawnTower(currentTower.type, towerSpawn, playerID);
				return true;
			}
		}
		return false;
	}

	public bool SellTower(){
		if (hit.collider.gameObject.tag == "Tower") {
			Tower tower = hit.collider.gameObject.GetComponent<Tower>();
			player.funds += tower.price / 2;
			CmdDestroyTower(tower.gameObject.transform.position);
			return true;
		}
		return false;
	}
    #endregion

    #region Node Functions
    bool GetCurrentNodes() {
        if (Grid.GetNode(hit.point) != null) {
            instanPos = GetMiddleOfNodes(hit.point);
            currentNodes = GetGridNodes(instanPos, Grid.nodeRadius);
            foreach(Vector3 cn in currentNodes) {
                GridNode gn = Grid.GetNode(cn);
                if (!gn.walkable) return false;
            }
        }
        return true;
    }
    private bool IsWalkableNodes(List<Vector3> nodes) {
        foreach (Vector3 node in nodes) {
            if (!Grid.GetNode(node).walkable) {
                return false;
            }
        }
        return true;
    }
    private List<Vector3> GetGridNodes(Vector3 _instanPos, float nodeRadius) {
        List<Vector3> nodes = new List<Vector3>();
        Vector3 nodePos = Vector3.zero;

        nodePos.x = _instanPos.x + nodeRadius;
        nodePos.z = _instanPos.z + nodeRadius;
        nodes.Add(nodePos);
        nodePos.x = _instanPos.x + nodeRadius;
        nodePos.z = _instanPos.z - nodeRadius;
        nodes.Add(nodePos);
        nodePos.x = _instanPos.x - nodeRadius;
        nodePos.z = _instanPos.z + nodeRadius;
        nodes.Add(nodePos);
        nodePos.x = _instanPos.x - nodeRadius;
        nodePos.z = _instanPos.z - nodeRadius;
        nodes.Add(nodePos);

        return nodes;
    }
    private Vector3 GetMiddleOfNodes(Vector3 hit) {
        GridNode currentNode = Grid.GetNode(hit);
        Vector3 offset = hit - currentNode.worldPos;
        Vector3 middle = currentNode.worldPos;
        if (offset.x >= 0 && offset.z >= 0) {
            middle.x += Grid.nodeRadius;
            middle.z += Grid.nodeRadius;
        } else if (offset.x >= 0 && offset.z < 0) {
            middle.x += Grid.nodeRadius;
            middle.z -= Grid.nodeRadius;
        } else if (offset.x < 0 && offset.z >= 0) {
            middle.x -= Grid.nodeRadius;
            middle.z += Grid.nodeRadius;
        } else if (offset.x < 0 && offset.z < 0) {
            middle.x -= Grid.nodeRadius;
            middle.z -= Grid.nodeRadius;
        }

        return middle;
    }
    #endregion

    #region Tower Functions
    [Command]
    public void CmdSpawnTower(string _type, Vector3 _pos, string _playerID) {
        Player p = GameManager.GetPlayer(_playerID);
        Tower tower = spawnableTowers[_type];
        if (p == default(Player)) {
            print("PlayerID invalid in CMDSpawnTower");
            return;
        }

		if (p.funds < tower.price) {
			print ("Not enough funds");
			return;
		} else {
			p.funds -= tower.price;
		}

        List<Vector3> nodes = GetGridNodes(_pos, Grid.nodeRadius);
        foreach (Vector3 node in nodes) {
            Vector2 gn = Grid.GetVector2(node);
            if(!Grid.grid[(int)gn.x, (int)gn.y].walkable) {
                print("Cannot place a tower here");
                return;
            }
        }
        //above and below foreach functions need to be separated to avoid setting a node unwalkable before the above function is finished
        foreach (Vector3 node in nodes) {
            Vector2 gn = Grid.GetVector2(node);
            Grid.grid[(int)gn.x, (int)gn.y].walkable = false;
        }
        
        tower.transform.position = _pos;
        GameObject obj = tower.gameObject;
        obj = (GameObject)Instantiate(obj, _pos, Quaternion.identity);

        NetworkServer.Spawn(obj);
        string ID = obj.GetComponent<NetworkIdentity>().netId.ToString();
        obj.name = _type + " " + ID;
        tower = obj.GetComponent<Tower>();
        tower.nodes = nodes;

        TowerManager.towers.Add(ID, tower);
    }

    [Command]
    public void CmdDestroyTower(Vector3 _pos) {
        Tower tower = default(Tower);
        string ID = "";
        foreach (KeyValuePair<string, Tower> kp in TowerManager.towers) {
            if (kp.Value.transform.position == _pos) {
                tower = kp.Value;
                ID = kp.Key;
                break;
            }
        }
        if (tower != default(Tower)) {
            foreach (Vector3 node in tower.nodes) {
                Vector2 gn = Grid.GetVector2(node);
                Grid.grid[(int)gn.x, (int)gn.y].walkable = true;
            }
            TowerManager.towers.Remove(ID);
            Destroy(tower.gameObject);
        }

    }
    #endregion

    private Camera FindCamera() {
        if (GetComponent<Camera>()) {
            return GetComponent<Camera>();
        }

        return Camera.main;
    }

	public void CycleTowerUp(){
		if (!(currentTowerTypeIndex + 1 > currentTowerTypes.Length - 1)) {
			currentTowerTypeIndex += 1;
		}
	}

	public void CycleTowerDown(){
		if (!(currentTowerTypeIndex - 1 < 0)) {
			currentTowerTypeIndex -= 1;
		}
	}
}
