﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Networking;
using System;
using System.IO;

/*
 * Maily keeps track of the players - and also match settings
 * Since there is only one Game manger instance per scene, this has been made as a singleton
 * 
*/
[System.Serializable]
public class GameManager : NetworkBehaviour
{
	/// <summary>
	/// These are what the game will track:
	/// </summary>
	/// 
	public int NumberOfEnemiesKilled;
	//public int NumberOfTowersDestroyed; //possible?
	public int DamageDoneByPlayer;
	public int DamageDoneByTowers;
	public float TimeSpentInGame;
	public int DamageDoneBySniper;
	public int DamageDoneByShotGun;
	public int DamageDoneByHandGun;
	public int DamageDontByAssultRifle;
	public int MoneySpentOntowers;
	public int MoneySpentOnWeapons;
	public int MoneySpentOnAmmo;
	public int WaveReached;
	public float timeSpentInBuildPhase;
	//set or controlled? enter to shortent wave to 5seconds
	public float AverageTimePerWave;
	//measre ach wave and avaerage it.

	//ALL again but for total
	//
	//
	//
	//
	//
	//
	//
	// or have a reset button




	public static GameManager singleton;

	private const string FILE_NAME = "ALLUSERDATA.txt";


	public MatchSettings matchsettings;

	void Awake ()
	{
		if (singleton != null) {
			Debug.LogError ("More than one Game manager is running"); 
		} else {
			singleton = this; 
			//singletons make it very easy to gain access to the Gamemanager without any gameobject.find or similar things. 
		}
	}

	void Start ()
	{

		if (isServer) {
			OpenFile ();			
		}



		String line = "";
		using (FileStream fs = new FileStream (FILE_NAME, FileMode.Open, FileAccess.Read)) {
			using (BinaryReader r = new BinaryReader (fs)) {
				//for (int i = 0; i <= 1; i++) { // 11?
					//print (r.ReadString ());	
				line = r.ReadString (); 
				print (line); 
				//}
			}
		}

		String[] data = line.Split(',');
		SortData (data);
	}

	public void SortData (String[] data)
	{
		NumberOfEnemiesKilled = int.Parse (data [0]); 
		DamageDoneByPlayer = int.Parse(data[1]);
		DamageDoneByTowers = int.Parse (data [2]);
		TimeSpentInGame = float.Parse (data [3]); 
		DamageDoneBySniper = int.Parse (data [4]);
		DamageDoneByShotGun = int.Parse (data [5]);
		DamageDoneByHandGun = int.Parse (data [6]);
		DamageDontByAssultRifle = int.Parse (data [7]);
		MoneySpentOntowers = int.Parse (data [8]);
		MoneySpentOnWeapons = int.Parse (data [9]);
		MoneySpentOnAmmo = int.Parse (data [10]);
		WaveReached = int.Parse (data [11]);
		timeSpentInBuildPhase = float.Parse(data[12]); //set or controlled? enter to shortent wave to 5seconds 
		AverageTimePerWave = float.Parse(data[13]); 			
	}

	public void OpenFile ()
	{
		if (File.Exists (FILE_NAME)) { //Check to see if the file is there
			print ("the file has landed" + FILE_NAME);
			return; 
		} else {
			//if not, create a new file that can have all the things. 
			resetData();
			//saveFile ();
			
		}
		
	}


	public void resetData()
	{
		print ("This is reset"); 
		NumberOfEnemiesKilled = 0;
		DamageDoneByPlayer = 0;
		DamageDoneByTowers = 0;
		TimeSpentInGame = 0;
		DamageDoneBySniper = 0;
		DamageDoneByShotGun = 0;
		DamageDoneByHandGun = 0;
		DamageDontByAssultRifle = 0;
		MoneySpentOntowers = 0;
		MoneySpentOnWeapons = 0;
		MoneySpentOnAmmo = 0;
		WaveReached = 0;
		timeSpentInBuildPhase = 0; //set or controlled? enter to shortent wave to 5seconds 
		AverageTimePerWave = 0;

		saveFile ();
	}

	public void saveFile()
	{
		string data = NumberOfEnemiesKilled + "," + DamageDoneByPlayer + "," +
		              DamageDoneByTowers + "," + TimeSpentInGame + "," +
		              DamageDoneBySniper + "," + DamageDoneByShotGun + "," +
		              DamageDoneByHandGun + "," + DamageDontByAssultRifle + "," +
		              MoneySpentOntowers + "," + MoneySpentOnWeapons + "," +
		              MoneySpentOnAmmo + "," + WaveReached + "," +
		              timeSpentInBuildPhase + "," + AverageTimePerWave;

		print ("saving this: "+data); 

		using (FileStream fs = new FileStream (FILE_NAME, FileMode.Create)) {
			using (BinaryWriter bw = new BinaryWriter (fs)) {
				//for (int i = 0; i < 11; i++) { // 11?
				bw.Write (data);							
				//}
			}
		}			
	}
		

	#region GameStateController+UPDATEMETHOD

	[SyncVar (hook = "StateChange")]
	public int CURRENT_STATE_INT = 0;
	public const int PHASE_BUILDING = 0;
	public const int PHASE_FIGHTING = 1;
	public const int PAUSED = 2;
	public const int WON = 3;
	public const int LOST = 4;

	public enum GameState
	{
		PHASE_BUILDING,
		PHASE_FIGHTING,
		PAUSED,
		WON,
		LOST}

	;

	[SyncVar]
	public GameState state = GameState.PHASE_BUILDING;
	private bool firstPhase = true;
	public bool enterPressed = false;
	[SerializeField]
	private float defaultNextPhaseCountdown = 5f;
	[SyncVar]
	public  float nextPhaseCountdown = 0;
	public static float buildTime = 90f;

	public bool gameOver = false; 

	void StateChange (int newState)
	{
		state = (GameState)newState;
		nextPhaseCountdown = defaultNextPhaseCountdown;
	}

	void Update ()
	{
		if (!isServer)
			return;
		if (state == GameState.PHASE_BUILDING && firstPhase && enterPressed) {
			nextPhaseCountdown = defaultNextPhaseCountdown;
			firstPhase = false;
		} else if (state == GameState.PHASE_BUILDING && !firstPhase) {
			//if enter key is pressed in normal build phase then speed up timer by setting it to 5 seconds
			if (nextPhaseCountdown > 5 && enterPressed) {
				nextPhaseCountdown = 5;
			}
			//keep the phase countdown ticking down
			if (nextPhaseCountdown > 0) {
				nextPhaseCountdown -= Time.deltaTime;
			}
			if (nextPhaseCountdown <= 0) { //if there is no time on the countdown transition into fighting phase must be if not elseif
				state = GameState.PHASE_FIGHTING;
			}
		} else if (state == GameState.LOST) {
			
			if (!gameOver) {
				print ("try to save file!"); 
				saveFile ();
				gameOver = true; 
			}

			
		}
		enterPressed = false;
	}

	#endregion

	/// <summary>
	/// region are easy to keep track of sections of code - the first region here is for
	/// the player code
	/// </summary>
	#region player tracking 
	private const string PLAYER_ID_PREFIX = "Player ";

	public static Dictionary<string, Player> players = new Dictionary<string, Player> ();


	/*
	 * This is so each player adds the id from network idenity to its name so that
	 * it is displayed. RegisterPlayer can be used to create a dictionary of all the
	 * players in the session
	 * 
	*/
	public static bool IsPlayer ()
	{
		if (players.Count > 0) {
			return true;
		}
		return false;
	}

	public static void RegisterPlayer (string _netID, Player _player)
	{
		string _playerID = PLAYER_ID_PREFIX + _netID; 
		players.Add (_playerID, _player);
		_player.transform.name = _playerID; //sets the name of the game object to the ID
	}

	public static void DeRegisterPlayer (string _playerID)
	{
		players.Remove (_playerID);
	}

	//you can get a player from this getter
	public static Player GetPlayer (string _playerID)
	{
		if (players.ContainsKey (_playerID))
			return players [_playerID];
		print ("Couldn't find enemy with ID: " + _playerID);
		return default(Player);
	}

	//Find the closest player to position
	public static Player GetClosestPlayer (Vector3 position)
	{
		Player currentClosest = default(Player);
		float currentClosestDistance = 0f;
		foreach (Player player in players.Values) {
			float distance = Vector3.Distance (player.gameObject.transform.position, position);
			if (distance < currentClosestDistance || currentClosestDistance == 0f) {
				currentClosest = player;
				currentClosestDistance = distance;
			}
		}
		if (currentClosest != default(Player)) {
			return currentClosest;
		}
		Debug.LogError ("No player found. Returned default player likely breaking script using this method");
		return default(Player);
	}

	public static List<Player> GetPlayersInRange (Vector3 position, float range)
	{
		List<Player> list = new List<Player> ();
		foreach (Player player in players.Values) {
			float distance = Vector3.Distance (player.gameObject.transform.position, position);
			if (distance <= range) {
				list.Add (player);
			}
		}
		return list;
	}


	//if you want to visualise the dictionary of players it can be done from this script
	//I havent done it but if you want me to, I can work on it

	//This is just for testing - SHOULD BE COMMENTED OUT
	/*
	void OnGUI () note: found bug... the gun seems to be player 1. O.o
	{
	    GUILayout.BeginArea(new Rect(200, 200, 200, 500));
	    GUILayout.BeginVertical();

	    foreach (string _playerID in players.Keys)
	    {
	        GUILayout.Label(_playerID + "  -  " + players[_playerID].transform.name);
	    }

	    GUILayout.EndVertical();
	    GUILayout.EndArea();
	}
	*/

	#endregion



}
