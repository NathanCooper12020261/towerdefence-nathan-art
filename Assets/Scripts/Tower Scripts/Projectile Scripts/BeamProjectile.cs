﻿using UnityEngine;
using System.Collections;

public class BeamProjectile : BaseProjectile {

    public float beamLength = 10.0f;
    GameObject mlauncher;

    // Update is called once per frame
    void Update()
    {
        if (mlauncher)
        {
            GetComponent<LineRenderer>().SetPosition(0, mlauncher.transform.position);
            GetComponent<LineRenderer>().SetPosition(1, mlauncher.transform.position + (mlauncher.transform.forward * beamLength));
        }
    }

    public override void FireProjectile(GameObject launcher, GameObject target, int damage, float attackSpeed)
    {
        if (launcher)
        {
            mlauncher = launcher;
            transform.SetParent(mlauncher.transform);
        }
    }
}
