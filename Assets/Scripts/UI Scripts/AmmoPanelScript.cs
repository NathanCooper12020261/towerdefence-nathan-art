﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AmmoPanelScript : MonoBehaviour {

	//private Text ammoText;

	public GameObject currentAmmo;
	public GameObject extraAmmo;


	private Text currentAmmoText;
	private Text extraAmmoText;

	// Use this for initialization
	void Start () {
		//ammoText = this.GetComponent<Text> ();	
		currentAmmoText = currentAmmo.GetComponent<Text>();
		extraAmmoText = extraAmmo.GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (currentAmmo != null) {
			currentAmmoText = currentAmmo.GetComponent<Text> ();
		}
		if (extraAmmo != null) {
			extraAmmoText = extraAmmo.GetComponent<Text> ();
		}
	}



	public void UpdateAmmoText(int amount, int extra){
		currentAmmoText.text = "" + amount;
		extraAmmoText.text = "" + extra;
	}




	public void UpdateAmmoText(string text, string extraText){
		currentAmmoText.text = text;
		extraAmmoText.text = extraText;
	}

}
