﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections.Generic;

[RequireComponent(typeof(WeaponManger))]
public class PlayerShoot : NetworkBehaviour
{


    [SerializeField]
    public BaseWeapon currentWeapon; //should this be synced? - no dont think so - R

    [SerializeField]
    private WeaponManger weaponManager;

    [SerializeField]
    public GameObject bullethole = null;

    [SerializeField]
    public GameObject hiteffect = null;

    //Note: I can make the gun on a seperate layer, then have it rendered by another camera
    //and have that camera go ontop of the current camera
    //what this would do is basicly prevent the gun clipping in walls - dont know if its worth it yet
    //this would also mean I can make the gun look very close - like in FPS games
    //while the model could hold the gun at a more reasonable range

    //convention for tags
    private const string PLAYER_TAG = "Player";

    private const string ENEMY_TAG = "Enemy";


    //public GameObject model;


    private Animator m_Animator;


    Ray ray;
    //control what we hit - for example freindly players.
    [SerializeField]
    private LayerMask mask;
    Camera mainCamera;

    HitMarkerScript hitmarker;

    // Use this for initialization
    void Start()
    {
        hitmarker = GameObject.FindGameObjectWithTag("Hitmarker").GetComponent<HitMarkerScript>();

        //Debug.Log ("The gun is active");
        if (!isLocalPlayer)
        {
            return;
        }
        Debug.Log("shoot start complete for player");
        mainCamera = FindCamera();
        weaponManager = GetComponent<WeaponManger>();

        //model = this.gameObject.transform.GetChild(0).gameObject;
        m_Animator = GetComponent<Animator>();//model.
        print("anmiator" + m_Animator);
        //print ("modele " + model);

    }
    private Camera FindCamera()
    {
        if (GetComponent<Camera>())
        {
            return GetComponent<Camera>();
        }

        return Camera.main;
    }
    [Command]
    void CmdDebug(string s)
    {
        print(s);
    }
    [Command]
    void CmdSpawn(string s)
    {
        print(s);
        //EnemyManager.singleton.CmdSpawnEnemy(Enemy.PLACEHOLDER, new Vector3(0, .5f, 0), 5);
        EnemyManager.singleton.CmdSpawnEnemy(Enemy.FLYING, new Vector3(3, .5f, 0), 5);
        //EnemyManager.singleton.CmdSpawnEnemy(Enemy.FASTFLYING, new Vector3(-3, 5f, 0), 5);
    }

    //IMPORTANT: this must be a command as syncvars are only syncing from server TO all clients so you must effect the variable on the server side to make it sync up with the clients
    //at least thats what i figure

    //OLD
    /*[Command]
    void CmdSht(float dmg) {
        RaycastHit hit;
        Ray ray = new Ray(transform.position, transform.TransformDirection(Vector3.forward));
        DrawLine(ray.origin + ray.direction * 2f, ray.origin + ray.direction * 50f, Color.cyan, 5f);
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, 200,
                    Physics.DefaultRaycastLayers)) {
            if (hit.collider.GetComponent<BaseHealth>()) {
                hit.collider.GetComponent<BaseHealth>().TakeDamage(dmg);
            }
        }
    }
    */

    [Command]
    void CmdShoot(Vector3 _pos, Vector3 _dir, float _dmg, float _range)
    {
        RaycastHit hit;
        if (Physics.Raycast(_pos, _dir, out hit, _range,
            mask))
        {
            if (hit.collider.GetComponent<BaseHealth>())
            {
                hit.collider.GetComponent<BaseHealth>().TakeDamage(_dmg);

                //If enemy dead, give moneys
                if (hit.collider.gameObject.tag == "Enemy" && hit.collider.gameObject.GetComponent<Enemy>().isDead)
                {
                    PlayerHelper.GetPlayer().GetComponent<Player>().funds += hit.collider.gameObject.GetComponent<Enemy>().fundsWorth;
                }
            }
        }
        else Debug.LogError("client shot something which was not in the same place/not spawned in the server");
    }

    //Fixed to give greater range
    public void Shoot()
    {
        //ray = mainCamera.ScreenPointToRay(new Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y, 0));
        //ray = mainCamera.ScreenPointToRay(Input.mousePosition); //atmp 
        print("Raycast was called");

        //ray = new Ray(mainCamera.transform.position + mainCamera.transform.rotation.eulerAngles * 2, mainCamera.transform.rotation.eulerAngles);
        print("This " + mainCamera.transform.position);
        print("That " + mainCamera.transform.rotation * Vector3.forward);

        ray = new Ray(mainCamera.transform.position, mainCamera.transform.forward);

		//instanciate as child of enmeiesy in a command. 

		 		 
		RaycastHit hitHit;
        if (Physics.Raycast(ray, out hitHit, 10000, mask))
        {//currentWeapon.maxRange, Physics.DefaultRaycastLayers)) {

            CmdOnHit(hitHit.point, hitHit.normal);

            //GameObject hole = (GameObject)GameObject.Instantiate(bullethole, hitHit.point, Quaternion.FromToRotation(Vector3.up, hitHit.normal));
            //NetworkServer.Spawn(hole); 
            print("your inside a raycast");

            if (hitHit.collider.gameObject.GetComponent<BaseHealth>())
            {
                print("You either hit an enmemy or Player ------------------++++++++++++++++++++++");
                hitmarker.Hit();
                CmdBulletHole(hitHit.point, hitHit.normal, hitHit.collider.gameObject.name);
                CmdShoot(ray.origin, ray.direction, currentWeapon.damage, 10000);//currentWeapon.maxRange);
            } //else if (hitHit.transform.tag == "Ground"){//"backgroundItem") {
              //print ("placed a bullet hole");

            //GameObject hole = (GameObject)GameObject.Instantiate(bullethole, hitHit.point, Quaternion.FromToRotation(Vector3.up, hitHit.normal));
            //NetworkServer.Spawn(hole); 

            //}
        }

        UpdateShootingStatistics();

    }

    void shiftyzak(Vector3 position, Vector3 normal, Transform transform)
    {
        GameObject hole = (GameObject)GameObject.Instantiate(bullethole, position, Quaternion.FromToRotation(Vector3.up, normal));
        hole.transform.SetParent(transform);
        NetworkServer.Spawn(hole);
    }

    [Command]
    void CmdBulletHole(Vector3 position, Vector3 normal, string name)
    {
        GameObject hole = (GameObject)GameObject.Instantiate(bullethole, position, Quaternion.FromToRotation(Vector3.up, normal));

        if (EnemyManager.enemies.ContainsKey(name))
        {
            Enemy enney = EnemyManager.enemies[name];
            hole.transform.SetParent(enney.transform);
            NetworkServer.Spawn(hole);
            RpcDoBulletHole(hole);
        }
    }


    [ClientRpc]
    void RpcDoBulletHole(GameObject hole)
    {
        //GameObject hole = (GameObject)GameObject.Instantiate(bullethole, position, Quaternion.FromToRotation(Vector3.up, normal));
        //hitHit.collider.gameObject.name
        //hole.transform.SetParent (); 
        //NetworkServer.Spawn(hole);
        Destroy(hole, 10f);
        print("You have a bullet hole");
    }


    [Command]
    void CmdOnHit(Vector3 position, Vector3 normal)
    {
        RpcDoHitEffect(position, normal);
    }


    [ClientRpc]
    void RpcDoHitEffect(Vector3 position, Vector3 normal)
    {
        //if each weapons has a hit effect - then it is weaponManger.getcurrentWeapon().hiteffect
        GameObject _hitEffect = (GameObject)Instantiate(hiteffect, position, Quaternion.FromToRotation(Vector3.up, normal));
        Destroy(_hitEffect, 2f);
        print("itr should be there");

    }




    //TODO: make spread work
    public void ShotGunShoot()
    {
        ray = new Ray(mainCamera.transform.position, mainCamera.transform.forward);
        Shoot(ray, currentWeapon.damage / 4, currentWeapon.maxRange);
        Shoot(new Ray(ray.origin, Spread(ray.direction)), currentWeapon.damage / 4, currentWeapon.maxRange);
        Shoot(new Ray(ray.origin, Spread(ray.direction)), currentWeapon.damage / 4, currentWeapon.maxRange);
        Shoot(new Ray(ray.origin, Spread(ray.direction)), currentWeapon.damage / 4, currentWeapon.maxRange);
        UpdateShootingStatistics();
    }
    public void Shoot(Ray ray, float dmg, float range)
    {

        //instanciate as child of enmeiesy in a command. 
        RaycastHit hitHit;
        if (Physics.Raycast(ray, out hitHit, 10000, mask))
        {//currentWeapon.maxRange, Physics.DefaultRaycastLayers)) {

            CmdOnHit(hitHit.point, hitHit.normal);

            //GameObject hole = (GameObject)GameObject.Instantiate(bullethole, hitHit.point, Quaternion.FromToRotation(Vector3.up, hitHit.normal));
            //NetworkServer.Spawn(hole); 
            print("your inside a raycast");

            if (hitHit.collider.gameObject.GetComponent<BaseHealth>())
            {
                print("You either hit an enmemy or Player ------------------++++++++++++++++++++++");
                hitmarker.Hit();
                CmdBulletHole(hitHit.point, hitHit.normal, hitHit.collider.gameObject.name);
                CmdShoot(ray.origin, ray.direction, dmg, range);//currentWeapon.maxRange);
            } //else if (hitHit.transform.tag == "Ground"){//"backgroundItem") {
              //print ("placed a bullet hole");

            //GameObject hole = (GameObject)GameObject.Instantiate(bullethole, hitHit.point, Quaternion.FromToRotation(Vector3.up, hitHit.normal));
            //NetworkServer.Spawn(hole); 

            //}
        }
    }




    Vector3 Spread(Vector3 aim, float variance = 10, float distance = 5)
    {
        Vector3 v3;
        do
        {
            v3 = Random.insideUnitSphere;
        } while (v3 == aim || v3 == -aim);
        v3 = Vector3.Cross(aim, v3);
        v3 = v3 * Random.Range(0, variance);
        return aim * distance + v3;

    }
    void UpdateShootingStatistics()
    {
        Statistics.timeWithoutShots = 0;
        Statistics.shotsFired += 1;
    }
    [Command]
    void CmdTransitionFight()
    {
        GameManager.singleton.enterPressed = true;
    }
    void Update()
    {
        if (!isLocalPlayer)
        {
            //Debug.Log ("No network idenity found or is not the right player.");
            return; //this is stopping th ray cast from being called...
        }
        else {
            //these two bits of code are temporary as player shoot was buggy and needed to put debug spawn button on player somehwere
            if (Input.GetKeyDown(KeyCode.F))
            {
                CmdSpawn("spawn");
            }
            if (Input.GetKeyDown(KeyCode.Return))
            {
                CmdTransitionFight();
            }
            currentWeapon = weaponManager.GetCurrentWeapon();
            if (currentWeapon.damage <= 0) return;
            if (currentWeapon != null)
            {
                //Changed
                //if (Input.GetButtonDown ("Fire1") && GlobalAmmo.currentAmmo > 0 && !currentWeapon.gunAnim.isPlaying) {
                if (Input.GetButtonDown("Fire1") && !currentWeapon.gunAnim.isPlaying)
                {
                    Debug.Log("shoot");
                    //Shoot ();

                    //This is where the player aniamtion for shoot is - it needs work...
                    m_Animator.SetTrigger("playerShoot");
                    //m_Animator.Play("playerShoot");

                    //print ("shoo! " + m_Animator);



                }
                else {
                    //The stats code
                    //m_Animator.SetBool ("playerShoot", false);
                    Statistics.timeWithoutShots += Time.deltaTime;
                    if (Statistics.timeWithoutShots > 5)
                    {
                        Statistics.shotsFired = 0;
                    }
                }


            }
        }
    }


    #region temp
    void DrawLine(Vector3 start, Vector3 end, Color color, float duration = 0.2f)
    {
        GameObject myLine = new GameObject();
        myLine.transform.position = start;
        myLine.AddComponent<LineRenderer>();
        LineRenderer lr = myLine.GetComponent<LineRenderer>();
        lr.material = new Material(Shader.Find("Particles/Alpha Blended Premultiply"));
        lr.SetColors(color, color);
        lr.SetWidth(0.1f, 0.1f);
        lr.SetPosition(0, start);
        lr.SetPosition(1, end);
        GameObject.Destroy(myLine, duration);
    }
    #endregion
}
